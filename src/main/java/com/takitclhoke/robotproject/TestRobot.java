/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.robotproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0, 0, 12, 6, 100);
        System.out.println(robot);
        robot.walk('E');
        System.out.println(robot);
        robot.walk('S', 3);
        System.out.println(robot);
        robot.walk('S', 4);
        System.out.println(robot);
        robot.walk('W');
        System.out.println(robot);
        robot.walk('E', 5);
        System.out.println(robot);
        robot.walk('E', 3);
        System.out.println(robot);
        robot.walk('N', 2);
        System.out.println(robot);
        robot.walk('E', 4);
        System.out.println(robot);
        robot.walk('S', 1);
        System.out.println(robot);
       
        
    }   
}
